+++
title = "GitLab sur un serveur peu puissant"
date = 2021-04-07

[taxonomies]
categorie = ["IT"]
tag = ["SysAdmin", "GitLab", "zRAM"]
+++

## Environnement utilisé 
Un CPX21 de [l’offre Cloud de Hetzner](https://www.hetzner.com/cloud). Je pense que les modifications restent intéressantes pour toute autre configuration équivalente ou moins puissante que celle-ci. Le serveur tourne sous Debian 10.

## Pré-requis 
Avoir déjà configuré son serveur SSH pour se connecter avec un utilisateur ayant accès aux droits d’administration.

## Installer et configurer zRAM 
zRAM est un outil permettant de compresser la RAM, pour gagner en mémoire vive disponible. Pour plus de détails, je vous recommande de lire l’introduction de [cette page de documentation](https://doc.ubuntu-fr.org/zram).

On installe le paquet.
```shell
sudo apt install zram-tools
```
Une fois installé, zRAM se lance tout seule avec une configuration par défaut. Pour configurer zRAM, on édite le fichier `/etc/default/zramswap`. On modifie la valeur `PERCENTAGE` pour augmenter la capacité que peut gérer zRAM, la valeur par défaut devrait être 10, on la passe à 100. On relance le service. pour que la modification fasse effet.
```shell
sudo systemctl restart zramswap.service
```

## Installer GitLab
Suivre [cette documentation](https://about.gitlab.com/install/#debian)

## Limiter le nombre de processus
Par défaut, GitLab fait tourner un nombre important de processus, ce qui fait fortement ralentir les serveurs peu puissants. Pour résoudre ce problème, on va éditer `/etc/gitlab/gitlab.rb` et réduire le nombre de processus simultanés utilisés par les services de GitLab.
```ruby
[…]
puma['worker_processes']=1
[…]
sidekiq['max_concurrency']=5
```
On relance GitLab pour que les modifications soient prises en compte.
```shell
sudo gitlab-ctl reconfigure
```
Une fois GitLab relancé, tout devrait fonctionner correctement et l’utilisation de la RAM du serveur ne devrait pas exceder 90%.

## Sources
- [Faire fonctionner GitLab sur un Raspberry Pi (en)](https://docs.gitlab.com/omnibus/settings/rpi.html)
