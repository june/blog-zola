+++
title = "Créer un utilisateur SFTP de manière sécurisée et minimaliste"
date = 2020-12-09

[taxonomies]
categorie = ["IT"]
tag = ["SysAdmin", "sftp", "linux"]
+++

## Pré-requis

Avoir déjà configuré son serveur SSH pour se connecter avec un utilisateur ayant accès aux droits d’administration.

## Mettre en place l’environnement

**Tout ce qui est indiqué dans cette partie a lieu sur le serveur**

Pour notre exemple, nous allons assumer que vous souhaitez créer un utilisateur qui va publier du contenu web. Les répertoires indiqués le sont à titre d’exemples, adaptez les chemins en conséquence de vos besoins.

### Créer l’utilisateur et son groupe

On créé l’utilisateur, puis son groupe, on rajoute l’utilisateur au groupe et enfin, on s’assure qu’il ne puisse accéder qu’au client SFTP.

```sh
sudo useradd <username>
sudo groupadd <groupname>
sudo gpasswd -a <username> <groupname>
sudo usermod -s  /usr/lib/openssh/sftp-server <username>
```

### Créer le répertoire de l’utilisateur

On créé le répertoire, on s’assure qu’il appartiennent bien à l’utilisateur `root`, de le protégér d’écriture de la part d’autres utilisateurs, puis on le définit comme répertoire d’accueil de l’utilisateur.

```sh
sudo mkdir /srv/www/domain.tld/
sudo chown root /srv/www/domain.tld
sudo chmod go-w /srv/www/domain.tld
sudo usermod -d /srv/www/domain.tld/ <username>
```

### Créer le répertoire web

C’est dans ce répertoire que l’utilisateur aura les droits pour créer, supprimer et éditer des fichiers et dossiers. On créé le répertoire, on nomme l’utilisateur et son groupe propriétaires puis on leur donne les droits nécessaires.

```sh
sudo mkdir /srv/www/domain.tld/public_html
sudo chown <username>:<groupname> /srv/www/domain.tld/public_html
sudo chmod ug+rwX /srv/www/domain.tld/public_html
```

### Finaliser l’environnement

N’oubliez pas de rajouter la clef publique SSH de votre utilisateur dans le dossier nécessaire.

On créé le répertoire `.ssh` nécessaire et le fichier `authorized_keys` dans lequel on va mettre la clef publique, enfin, on rend l’utilisateur propriétaire et on s’assure qu’il puisse lire le fichier.

```sh
sudo mkdir /srv/www/domain.tld/.ssh
sudo touch /srv/www/domain.tld/.ssh/authorized_keys
sudo chown -R <username>:<groupname> /srv/www/domain.tld/.ssh
sudo chmod u+rX /srv/www/domain.tld/.ssh
```

Il n’y a plus qu’à copier-coller la clef publique SSH dans le fichier `authorized_keys`.

## Bloquer l’utilisateur dans l’environnement

**Tout ce qui est indiqué dans cette partie a lieu sur le serveur**

On édite le fichier `/etc/ssh/sshd_config` on vérifie si la directive `Subsystem` est bien comme indiquée ci-dessous, sinon, on la corrige ou la créé.

```
[…]
Subsystem sftp internal-sftp
[…]
```

On cherche le bloc `AllowUsers`, s’il est commenté (avec un en début de ligne `#`), on le décommente (on efface le `#`) et puis on y ajoute notre utilisateur.

```
[…]
AllowUsers <username>
[…]
```

On se rend à la fin du fichier de configuration, et on rajoute la partie suivante.

```
[…]
Match User <username> # On indique que notre partie concerne l’utilisateur
	ChrootDirectory /srv/www/domain.tld # Là où on veut enfermer l’utilisateur
	AllowTCPForwarding no
	X11Forwarding no
	ForceCommand internal-sftp # On force l’utilisateur à utiliser seulement le terminal SFTP
```

On redémarre SSH pour que la nouvelle configuration soit prise en compte.

```sh
sudo systemctl restart sshd.service
```

## Côté client

Sur le PC de l’utilisateur·trice, [on génère simplement la clef](https://tutox.fr/2020/04/16/generer-des-cles-ssh-qui-tiennent-la-route/), et on utilisera SFTP pour se connecter de manière sécurisée.

### Tester la connexion à l’environnement

En essayant de se connecter via SSH avec l’utilisateur SFTP, on devrait obtenir le message suivant : `This service allows sftp connections only.`. SFTP devrait se connecter sans soucis, la commande `pwd` affichant `/`. Toute tentative d’ajouter du contenu dans le dossier racine devrait échouée mais une fois dans le dossier `/public_html`, l’utilisateur devrait pouvoir ajouter, éditer et supprimer des fichiers et dossiers.

## Sources

* [Créer l’utilisateur proprement (en)](https://unix.stackexchange.com/questions/83221/how-to-create-a-ftp-user-with-specific-dir-access-only-on-a-centos-linux-ins)
* [Placer l’utilisateur et son dossier web (en)](https://askubuntu.com/questions/134425/how-can-i-chroot-sftp-only-ssh-users-into-their-homes)
* [Restreindre l’utilisateur à l’environnement SFTP (en)](https://www.howtoforge.com/restricting-users-to-sftp-plus-setting-up-chrooted-ssh-sftp-debian-squeeze)

## Pour aller plus loin…

### Créer un seul groupe pour les utilisateurs SFTP

Si on doit créer plusieurs utilisateurs, avoir un seul groupe à gérer peut être plus pratique. On peut par exemple l’appeler `sftp`. À la création de nouveaux utilisateurs, on n’aura qu’à directement ajouter ces derniers au groupe comme indiqué au début de ce billet.

### Un lien pour approfondir

* ["How to Set File Permissions Using `chmod`" par l’*Université de Washington*(en)](https://www.washington.edu/computing/unix/permissions.html)
