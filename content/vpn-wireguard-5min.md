+++
title = "Mettre en place un VPN avec WireGuard en 5 minutes"
date = 2021-05-19

[taxonomies]
categorie = ["IT"]
tag = ["WireGuard", "VPN"]
+++

## Edito
Cet article a été rédigé et publié en quelques minutes, son accessibilité est moins travaillée que d’habitude.

## Environnement
* OS serveur : Debian 10 **à jour**
*  OS client : Distribution GNU/Linux **à jour**
* Un utilisateur avec les droits sudo sur chaque machine

## Côté serveur #1
```shell
sudo apt install wireguard
wg genkey | tee private_key | wg pubkey > public_key
sudo mkdir /etc/wireguard/helper
```

Créer et éditer le fichier de configuration du serveur, pour cet exemple, c’est **wg0.conf**.

```conf
[Interface]
Address = 10.200.200.1/24
ListenPort = 51821
PrivateKey = [coller ici le contenu du fichier 'private_key' généré précedemment]
PostUp = /etc/wireguard/helper/add-nat-routing.sh
PostDown = /etc/wireguard/helper/remove-nat-routing.sh

```

Créer et éditer le fichier **/etc/wireguard/helper/add-nat-routing.sh**.

```shell
#!/bin/bash
IPT="/sbin/iptables"
IPT6="/sbin/ip6tables"          
 
IN_FACE="eth0"                   # interface réseau connectée à Internet
WG_FACE="wg0"                    # interface réseau connectée à WireGuard
SUB_NET="10.200.200.0/24"            # sous-réseau de WireGuard en IPv4
WG_PORT="51821"                  # port UDP utilisé par WireGuard 
SUB_NET_6="fd42:42:42:42::/112"  # sous-réseau de WireGuard en IPv6
 
## règles IPv4 ##
$IPT -t nat -I POSTROUTING 1 -s $SUB_NET -o $IN_FACE -j MASQUERADE
$IPT -I INPUT 1 -i $WG_FACE -j ACCEPT
$IPT -I FORWARD 1 -i $IN_FACE -o $WG_FACE -j ACCEPT
$IPT -I FORWARD 1 -i $WG_FACE -o $IN_FACE -j ACCEPT
$IPT -I INPUT 1 -i $IN_FACE -p udp --dport $WG_PORT -j ACCEPT
 
## IPv6 (à décommenter si utilisation d’IPv6, non testé) ##
## $IPT6 -t nat -I POSTROUTING 1 -s $SUB_NET_6 -o $IN_FACE -j MASQUERADE
## $IPT6 -I INPUT 1 -i $WG_FACE -j ACCEPT
## $IPT6 -I FORWARD 1 -i $IN_FACE -o $WG_FACE -j ACCEPT
## $IPT6 -I FORWARD 1 -i $WG_FACE -o $IN_FACE -j ACCEPT
```

Créer et éditer le fichier **/etc/wireguard/helper/remove-nat-routing.sh**.

```shell
#!/bin/bash
IPT="/sbin/iptables"
IPT6="/sbin/ip6tables"          
 
IN_FACE="eth0"                   # interface réseau connectée à Internet
WG_FACE="wg0"                   # interface réseau connectée à WireGuard
SUB_NET="10.200.200.0/24"            # sous-réseau de WireGuard en IPv4
WG_PORT="51821"                 # port UDP utilisé par WireGuard 
SUB_NET_6="fd42:42:42:42::/112"  # sous-réseau de WireGuard en IPv6
 
# règles IPv4 #
$IPT -t nat -D POSTROUTING -s $SUB_NET -o $IN_FACE -j MASQUERADE
$IPT -D INPUT -i $WG_FACE -j ACCEPT
$IPT -D FORWARD -i $IN_FACE -o $WG_FACE -j ACCEPT
$IPT -D FORWARD -i $WG_FACE -o $IN_FACE -j ACCEPT
$IPT -D INPUT -i $IN_FACE -p udp --dport $WG_PORT -j ACCEPT
 
# règles IPv6 (à décommenter si utilisation d’IPv6, non testé) #
## $IPT6 -t nat -D POSTROUTING -s $SUB_NET_6 -o $IN_FACE -j MASQUERADE
## $IPT6 -D INPUT -i $WG_FACE -j ACCEPT
## $IPT6 -D FORWARD -i $IN_FACE -o $WG_FACE -j ACCEPT
## $IPT6 -D FORWARD -i $WG_FACE -o $IN_FACE -j ACCEPT
```

Rendre les scripts exécutables

```shell
sudo chmod +x /etc/wireguard/helper/*.sh
```

Créer et éditer le fichier **/etc/sysctl.d/10-wireguard.conf**

```conf
net.ipv4.ip_forward=1
net.ipv6.conf.all.forwarding=1
```

Prendre en compte les modifications de sysctl.d

```shell
sysctl -p /etc/sysctl.d/10-wireguard.conf
```

Supprimer le fichier **private_key**

## Côté client #1
```shell
wg genkey | tee private_key | wg pubkey > public_key
```

Créer et éditer le fichier de configuration client de WireGuard, dans cet exemple **wg0-client.conf**.

```conf
[Interface]
Address = 10.200.200.2/32
PrivateKey = [coller ici le contenu du fichier 'private_key' **généré côté client**]
DNS = 80.67.169.12, 80.67.169.40

[Peer]
PublicKey = [coller ici le contenu du fichier 'public_key' **généré côté serveur**
Endpoint = [adresse IP Internet du serveur]:51821
AllowedIPs = 0.0.0.0/0
PersistentKeepalive = 21
```

Supprimer le fichier **private_key**.

## Côté serveur #2
Éditer **/etc/wireguard/wg0.conf**.

```conf
[…]

[Peer]
PublicKey = [coller ici le contenu du fichier 'public_key' **généré côté client**]
AllowedIPs = 10.200.200.2/32
```

Démarrer WireGuard, puis l’activer au démarrage du serveur.

```shell
sudo wg-quick up wg0
sudo systemctl enable wg-quick@wg0.service
```

## Côté client #2
Démarrer WireGuard.

```shell
sudo wg-quick up wg0-client
```

## Source
* [https://www.cyberciti.biz/faq/how-to-set-up-wireguard-firewall-rules-in-linux/](https://www.cyberciti.biz/faq/how-to-set-up-wireguard-firewall-rules-in-linux/)
